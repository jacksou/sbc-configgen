<?php

/* SwisscomMatrixHomeBundle:Default:index.html.twig */
class __TwigTemplate_3805d8f4f8dfed59151eec8a4cab8e93 extends Twig_Template
{
    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        // line 1
        echo "Hello ";
        echo twig_escape_filter($this->env, $this->getContext($context, 'name'), "html");
        echo "!
";
    }

    public function getTemplateName()
    {
        return "SwisscomMatrixHomeBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
