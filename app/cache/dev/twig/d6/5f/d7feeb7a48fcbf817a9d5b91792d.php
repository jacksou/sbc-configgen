<?php

/* SwisscomMatrixHomeBundle:Default:hello.html.twig */
class __TwigTemplate_d65fd7feeb7a48fcbf817a9d5b91792d extends Twig_Template
{
    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <title>Welcome to Symfony!</title>
    </head>
    <body>
        <h1>";
        // line 7
        echo twig_escape_filter($this->env, $this->getContext($context, 'page_title'), "html");
        echo "</h1>

        <ul id=\"navigation\">
            ";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, 'navigation'));
        foreach ($context['_seq'] as $context['_key'] => $context['item']) {
            // line 11
            echo "                <li><a href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, 'item'), "href", array(), "any", false), "html");
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, 'item'), "caption", array(), "any", false), "html");
            echo "</a></li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 13
        echo "        </ul>
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "SwisscomMatrixHomeBundle:Default:hello.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
