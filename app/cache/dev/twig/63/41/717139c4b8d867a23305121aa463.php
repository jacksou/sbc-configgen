<?php

/* SwisscomMatrixConfigBundle:Default:index.html.twig */
class __TwigTemplate_6341717139c4b8d867a23305121aa463 extends Twig_Template
{
    protected $parent;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = array();
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    public function getParent(array $context)
    {
        $parent = "SwisscomMatrixConfigBundle:Template:SwisscomTemplate.html.twig";
        if ($parent instanceof Twig_Template) {
            $name = $parent->getTemplateName();
            $this->parent[$name] = $parent;
            $parent = $name;
        } elseif (!isset($this->parent[$parent])) {
            $this->parent[$parent] = $this->env->loadTemplate($parent);
        }

        return $this->parent[$parent];
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "SBC Matrix ConfigGen";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "Welcome to the Swisscom Broadcast config generator

";
    }

    public function getTemplateName()
    {
        return "SwisscomMatrixConfigBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
