<?php

/* SwisscomMatrixConfigBundle:Template:SwisscomTemplate.html.twig */
class __TwigTemplate_2e2b81c6d323ef4f6919d71039a0aa1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = array();
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $context = array_merge($this->env->getGlobals(), $context);

        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">
<title>SBC Matrix ConfigGen - ";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/swisscommatrixconfig/css/layout.css"), "html");
        echo "\" type=\"text/css\">
<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/swisscommatrixconfig/css/content.css"), "html");
        echo "\" type=\"text/css\">
<link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/swisscommatrixconfig/css/print.css"), "html");
        echo "\" type=\"text/css\"
\tmedia=\"print\">
<meta http-equiv=\"pragma\" content=\"no-cache\"><script
\tlanguage=\"javascript\">
\t  function openBrWindow1(theURL,winName,features) { 
\t    window.open(theURL,winName,features);
\t  }
\t </script><script type=\"text/javascript\">
\t    function adjustDividerHeight() {
\t    
\t      var navigation = document.getElementById('navigation');
\t      var container = document.getElementById('container');
\t      
\t      var height = Math.max(navigation.offsetHeight, container.offsetHeight) -130;

\t      var divider = document.getElementById('dividerLeft');
\t      if( divider != null) {
\t        divider.style.height = height + \"px\";
\t      }
\t      
\t      divider = document.getElementById('dividerRight');
\t      if( divider != null) {
\t        divider.style.height = height + \"px\";
\t      }
\t    }
\t</script>
</head>
<body onLoad=\"document.login.username.focus()\">
<div class=\"pageTop\">
<div id=\"printLogo\"><img alt=\"\" src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/swisscommatrixconfig/images/layout/ScLogo.gif"), "html");
        echo "\"></div>
<div class=\"page\">
<div class=\"header\">
<div class=\"headerContent\" id=\"headerContent\">
<div class=\"headerWelcome\">&nbsp;</div>
<div class=\"headerLinks\"></div>
</div>
<div class=\"headerLogo\" id=\"headerLogo\"><a target=\"_blank\"
\thref=\"http://www.swisscom.com/GHQ/content?lang=en\"><img
\tsrc=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/swisscommatrixconfig/images/layout/Primary_Loop_Small.gif"), "html");
        echo "\" alt=\"Swisscom\" width=\"100\"
\theight=\"75\" border=\"0\"></a></div>
</div>
<div class=\"containerTop\">
<div id=\"navigation\" class=\"navigation\">
<div class=\"home\"><a class=\"parent\" href=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("SwisscomMatrixConfigBundle_homepage"), "html");
        echo "\">
Home <img src=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/swisscommatrixconfig/images/layout/ArrowRoundRight.gif"), "html");
        echo "\" alt=\"\" width=\"14\"
\theight=\"14\" border=\"0\" hspace=\"2\"></a></div>
<div class=\"separator\"></div>
<div class=\"spacersmall\"></div>
<!-- Menu -->
 <div>
  <a class=\"textArrow\" href=\"#\">Create a config</a><br/><br/>
  ------- Admin  -------
 <br /><br/>
   <a class=\"textArrow\" href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("SwisscomMatrixConfigBundle_project"), "html");
        echo "\">Project </a><br/><br/>
   <a class=\"textArrow\" href=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("SwisscomMatrixConfigBundle_connection_type"), "html");
        echo "\">Type of connections </a><br /><br/>
   <a class=\"textArrow\" href=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("SwisscomMatrixConfigBundle_config_store"), "html");
        echo "\">Configuration store </a><br /><br/>
   <a class=\"textArrow\" href=\"";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("SwisscomMatrixConfigBundle_config_template"), "html");
        echo "\">Configuration Template </a><br /><br/>
   <a class=\"textArrow\" href=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("SwisscomMatrixConfigBundle_parameters_form"), "html");
        echo "\">Parameters Form </a><br /><br/>
   <a class=\"textArrow\" href=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("SwisscomMatrixConfigBundle_loopback"), "html");
        echo "\">Loopback </a><br /><br/>

      <br />
</div>
<!-- End of Menu -->


<div class=\"spacerwide\"></div>
</div>
<div id=\"container\" class=\"container\">
<div class=\"dividerLeft\">
<div class=\"dividerLeftTop\"></div>
<div id=\"dividerLeft\" class=\"dividerLeftMiddle\"></div>
<div class=\"dividerLeftBottom\"></div>
</div>
<div class=\"contentarea\" id=\"expand\">
<div style=\"padding-top: 26px;\" id=\"tour-promotional\"><img alt=\"\"
\twidth=\"685\" height=\"203\" border=\"0\"
\tsrc=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/swisscommatrixconfig/images/layout/KuCeLogin_Header_e.jpg"), "html");
        echo "\"></div>

<div class=\"loginBar\">
<div class=\"LoginTabSelected\">";
        // line 88
        $this->displayBlock("title", $context, $blocks);
        echo "</div>
<div class=\"LoginTabImageRight\"></div>
</div>
<br></br>
<!-- Content -->
      ";
        // line 93
        $this->displayBlock('content', $context, $blocks);
        // line 95
        echo "<!-- End of Content -->
<div class=\"dividerRight\">
<div class=\"dividerRightTop\"></div>
<div id=\"dividerRight\" class=\"dividerRightMiddle\"></div>
<div class=\"dividerRightBottom\"></div>
</div>
</div>
</div>
<div class=\"footer\">
<div class=\"copyright\">Swisscom
Broadcast&nbsp;(Switzerland)&nbsp;Ltd.&nbsp;| <a
\thref=\"www.swisscom.ch/SBC/content?lang=en\" target=\"_blank\">
www.swisscom.ch/SBC </a></div>
</div>
</div>
</div>
<script type=\"text/javascript\">adjustDividerHeight();</script>
</body>
</html>
";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
    }

    // line 93
    public function block_content($context, array $blocks = array())
    {
        // line 94
        echo "      ";
    }

    public function getTemplateName()
    {
        return "SwisscomMatrixConfigBundle:Template:SwisscomTemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }
}
