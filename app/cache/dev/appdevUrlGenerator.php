<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;


/**
 * appdevUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    static private $declaredRouteNames = array(
       '_welcome' => true,
       '_demo_login' => true,
       '_security_check' => true,
       '_demo_logout' => true,
       'acme_demo_secured_hello' => true,
       '_demo_secured_hello' => true,
       '_demo_secured_hello_admin' => true,
       '_demo' => true,
       '_demo_hello' => true,
       '_demo_contact' => true,
       '_wdt' => true,
       '_profiler_search' => true,
       '_profiler_purge' => true,
       '_profiler_import' => true,
       '_profiler_export' => true,
       '_profiler_search_results' => true,
       '_profiler' => true,
       '_configurator_home' => true,
       '_configurator_step' => true,
       '_configurator_final' => true,
       'SwisscomMatrixConfigBundle_homepage' => true,
       'SwisscomMatrixConfigBundle_project' => true,
       'SwisscomMatrixConfigBundle_project_new' => true,
       'SwisscomMatrixConfigBundle_project_create' => true,
       'SwisscomMatrixConfigBundle_project_edit' => true,
       'SwisscomMatrixConfigBundle_project_update' => true,
       'SwisscomMatrixConfigBundle_project_show' => true,
       'SwisscomMatrixConfigBundle_project_delete' => true,
       'SwisscomMatrixConfigBundle_connection_type' => true,
       'SwisscomMatrixConfigBundle_connection_type_new' => true,
       'SwisscomMatrixConfigBundle_connection_type_create' => true,
       'SwisscomMatrixConfigBundle_connection_type_edit' => true,
       'SwisscomMatrixConfigBundle_connection_type_update' => true,
       'SwisscomMatrixConfigBundle_connection_type_show' => true,
       'SwisscomMatrixConfigBundle_connection_type_delete' => true,
       'SwisscomMatrixConfigBundle_config_store' => true,
       'SwisscomMatrixConfigBundle_config_store_new' => true,
       'SwisscomMatrixConfigBundle_config_store_create' => true,
       'SwisscomMatrixConfigBundle_config_store_edit' => true,
       'SwisscomMatrixConfigBundle_config_store_update' => true,
       'SwisscomMatrixConfigBundle_config_store_show' => true,
       'SwisscomMatrixConfigBundle_config_store_delete' => true,
       'SwisscomMatrixConfigBundle_config_template' => true,
       'SwisscomMatrixConfigBundle_config_template_new' => true,
       'SwisscomMatrixConfigBundle_config_template_create' => true,
       'SwisscomMatrixConfigBundle_config_template_edit' => true,
       'SwisscomMatrixConfigBundle_config_template_update' => true,
       'SwisscomMatrixConfigBundle_config_template_show' => true,
       'SwisscomMatrixConfigBundle_config_template_delete' => true,
       'SwisscomMatrixConfigBundle_parameters_form' => true,
       'SwisscomMatrixConfigBundle_parameters_form_new' => true,
       'SwisscomMatrixConfigBundle_parameters_form_create' => true,
       'SwisscomMatrixConfigBundle_parameters_form_edit' => true,
       'SwisscomMatrixConfigBundle_parameters_form_update' => true,
       'SwisscomMatrixConfigBundle_parameters_form_show' => true,
       'SwisscomMatrixConfigBundle_parameters_form_delete' => true,
       'SwisscomMatrixConfigBundle_loopback' => true,
       'SwisscomMatrixConfigBundle_loopback_new' => true,
       'SwisscomMatrixConfigBundle_loopback_create' => true,
       'SwisscomMatrixConfigBundle_loopback_edit' => true,
       'SwisscomMatrixConfigBundle_loopback_update' => true,
       'SwisscomMatrixConfigBundle_loopback_show' => true,
       'SwisscomMatrixConfigBundle_loopback_delete' => true,
       'SwisscomMatrixHomeBundle_homepage' => true,
       'AcmeHelloBundle_homepage' => true,
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function generate($name, $parameters = array(), $absolute = false)
    {
        if (!isset(self::$declaredRouteNames[$name])) {
            throw new RouteNotFoundException(sprintf('Route "%s" does not exist.', $name));
        }

        $escapedName = str_replace('.', '__', $name);

        list($variables, $defaults, $requirements, $tokens) = $this->{'get'.$escapedName.'RouteInfo'}();

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $absolute);
    }

    private function get_welcomeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/',  ),));
    }

    private function get_demo_loginRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/secured/login',  ),));
    }

    private function get_security_checkRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/secured/login_check',  ),));
    }

    private function get_demo_logoutRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/secured/logout',  ),));
    }

    private function getacme_demo_secured_helloRouteInfo()
    {
        return array(array (), array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/secured/hello',  ),));
    }

    private function get_demo_secured_helloRouteInfo()
    {
        return array(array (  0 => 'name',), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'name',  ),  1 =>   array (    0 => 'text',    1 => '/demo/secured/hello',  ),));
    }

    private function get_demo_secured_hello_adminRouteInfo()
    {
        return array(array (  0 => 'name',), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'name',  ),  1 =>   array (    0 => 'text',    1 => '/demo/secured/hello/admin',  ),));
    }

    private function get_demoRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/',  ),));
    }

    private function get_demo_helloRouteInfo()
    {
        return array(array (  0 => 'name',), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'name',  ),  1 =>   array (    0 => 'text',    1 => '/demo/hello',  ),));
    }

    private function get_demo_contactRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/demo/contact',  ),));
    }

    private function get_wdtRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::toolbarAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/_wdt',  ),));
    }

    private function get_profiler_searchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/search',  ),));
    }

    private function get_profiler_purgeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::purgeAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/purge',  ),));
    }

    private function get_profiler_importRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::importAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/import',  ),));
    }

    private function get_profiler_exportRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::exportAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '.txt',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/\\.]+?',    3 => 'token',  ),  2 =>   array (    0 => 'text',    1 => '/_profiler/export',  ),));
    }

    private function get_profiler_search_resultsRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchResultsAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/search/results',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  2 =>   array (    0 => 'text',    1 => '/_profiler',  ),));
    }

    private function get_profilerRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::panelAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/_profiler',  ),));
    }

    private function get_configurator_homeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_configurator/',  ),));
    }

    private function get_configurator_stepRouteInfo()
    {
        return array(array (  0 => 'index',), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'index',  ),  1 =>   array (    0 => 'text',    1 => '/_configurator/step',  ),));
    }

    private function get_configurator_finalRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_configurator/final',  ),));
    }

    private function getSwisscomMatrixConfigBundle_homepageRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/',  ),));
    }

    private function getSwisscomMatrixConfigBundle_projectRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/project/',  ),));
    }

    private function getSwisscomMatrixConfigBundle_project_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/project/new',  ),));
    }

    private function getSwisscomMatrixConfigBundle_project_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::createAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/project/create',  ),));
    }

    private function getSwisscomMatrixConfigBundle_project_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/project',  ),));
    }

    private function getSwisscomMatrixConfigBundle_project_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::updateAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/project',  ),));
    }

    private function getSwisscomMatrixConfigBundle_project_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/project',  ),));
    }

    private function getSwisscomMatrixConfigBundle_project_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::deleteAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/project',  ),));
    }

    private function getSwisscomMatrixConfigBundle_connection_typeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/connection/type/',  ),));
    }

    private function getSwisscomMatrixConfigBundle_connection_type_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/connection/type/new',  ),));
    }

    private function getSwisscomMatrixConfigBundle_connection_type_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::createAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/connection/type/create',  ),));
    }

    private function getSwisscomMatrixConfigBundle_connection_type_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/connection/type',  ),));
    }

    private function getSwisscomMatrixConfigBundle_connection_type_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::updateAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/connection/type',  ),));
    }

    private function getSwisscomMatrixConfigBundle_connection_type_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/connection/type',  ),));
    }

    private function getSwisscomMatrixConfigBundle_connection_type_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::deleteAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/connection/type',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_storeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/store/',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_store_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/store/new',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_store_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::createAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/store/create',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_store_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/store',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_store_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::updateAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/store',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_store_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/store',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_store_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::deleteAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/store',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_templateRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/template/',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_template_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/template/new',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_template_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::createAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/template/create',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_template_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/template',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_template_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::updateAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/template',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_template_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/template',  ),));
    }

    private function getSwisscomMatrixConfigBundle_config_template_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::deleteAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/config/template',  ),));
    }

    private function getSwisscomMatrixConfigBundle_parameters_formRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/parameters/form/',  ),));
    }

    private function getSwisscomMatrixConfigBundle_parameters_form_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/parameters/form/new',  ),));
    }

    private function getSwisscomMatrixConfigBundle_parameters_form_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::createAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/parameters/form/create',  ),));
    }

    private function getSwisscomMatrixConfigBundle_parameters_form_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/parameters/form',  ),));
    }

    private function getSwisscomMatrixConfigBundle_parameters_form_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::updateAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/parameters/form',  ),));
    }

    private function getSwisscomMatrixConfigBundle_parameters_form_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/parameters/form',  ),));
    }

    private function getSwisscomMatrixConfigBundle_parameters_form_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::deleteAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/parameters/form',  ),));
    }

    private function getSwisscomMatrixConfigBundle_loopbackRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/loopback/',  ),));
    }

    private function getSwisscomMatrixConfigBundle_loopback_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/loopback/new',  ),));
    }

    private function getSwisscomMatrixConfigBundle_loopback_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::createAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/loopback/create',  ),));
    }

    private function getSwisscomMatrixConfigBundle_loopback_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/loopback',  ),));
    }

    private function getSwisscomMatrixConfigBundle_loopback_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::updateAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/loopback',  ),));
    }

    private function getSwisscomMatrixConfigBundle_loopback_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/loopback',  ),));
    }

    private function getSwisscomMatrixConfigBundle_loopback_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::deleteAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/Matrix/ConfigGen/loopback',  ),));
    }

    private function getSwisscomMatrixHomeBundle_homepageRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Swisscom\\Matrix\\HomeBundle\\Controller\\DefaultController::helloAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/Matrix/Hello',  ),));
    }

    private function getAcmeHelloBundle_homepageRouteInfo()
    {
        return array(array (  0 => 'name',), array (  '_controller' => 'Acme\\HelloBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'name',  ),  1 =>   array (    0 => 'text',    1 => '/hello',  ),));
    }
}
