<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appdevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = urldecode($pathinfo);

        // _welcome
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', '_welcome');
            }
            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\WelcomeController::indexAction',  '_route' => '_welcome',);
        }

        // _demo_login
        if ($pathinfo === '/demo/secured/login') {
            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::loginAction',  '_route' => '_demo_login',);
        }

        // _security_check
        if ($pathinfo === '/demo/secured/login_check') {
            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::securityCheckAction',  '_route' => '_security_check',);
        }

        // _demo_logout
        if ($pathinfo === '/demo/secured/logout') {
            return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::logoutAction',  '_route' => '_demo_logout',);
        }

        // acme_demo_secured_hello
        if ($pathinfo === '/demo/secured/hello') {
            return array (  'name' => 'World',  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',  '_route' => 'acme_demo_secured_hello',);
        }

        // _demo_secured_hello
        if (0 === strpos($pathinfo, '/demo/secured/hello') && preg_match('#^/demo/secured/hello/(?P<name>[^/]+?)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloAction',)), array('_route' => '_demo_secured_hello'));
        }

        // _demo_secured_hello_admin
        if (0 === strpos($pathinfo, '/demo/secured/hello/admin') && preg_match('#^/demo/secured/hello/admin/(?P<name>[^/]+?)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Acme\\DemoBundle\\Controller\\SecuredController::helloadminAction',)), array('_route' => '_demo_secured_hello_admin'));
        }

        if (0 === strpos($pathinfo, '/demo')) {
            // _demo
            if (rtrim($pathinfo, '/') === '/demo') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_demo');
                }
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::indexAction',  '_route' => '_demo',);
            }

            // _demo_hello
            if (0 === strpos($pathinfo, '/demo/hello') && preg_match('#^/demo/hello/(?P<name>[^/]+?)$#xs', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::helloAction',)), array('_route' => '_demo_hello'));
            }

            // _demo_contact
            if ($pathinfo === '/demo/contact') {
                return array (  '_controller' => 'Acme\\DemoBundle\\Controller\\DemoController::contactAction',  '_route' => '_demo_contact',);
            }

        }

        // _wdt
        if (preg_match('#^/_wdt/(?P<token>[^/]+?)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::toolbarAction',)), array('_route' => '_wdt'));
        }

        if (0 === strpos($pathinfo, '/_profiler')) {
            // _profiler_search
            if ($pathinfo === '/_profiler/search') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchAction',  '_route' => '_profiler_search',);
            }

            // _profiler_purge
            if ($pathinfo === '/_profiler/purge') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::purgeAction',  '_route' => '_profiler_purge',);
            }

            // _profiler_import
            if ($pathinfo === '/_profiler/import') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::importAction',  '_route' => '_profiler_import',);
            }

            // _profiler_export
            if (0 === strpos($pathinfo, '/_profiler/export') && preg_match('#^/_profiler/export/(?P<token>[^/\\.]+?)\\.txt$#xs', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::exportAction',)), array('_route' => '_profiler_export'));
            }

            // _profiler_search_results
            if (preg_match('#^/_profiler/(?P<token>[^/]+?)/search/results$#xs', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchResultsAction',)), array('_route' => '_profiler_search_results'));
            }

            // _profiler
            if (preg_match('#^/_profiler/(?P<token>[^/]+?)$#xs', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::panelAction',)), array('_route' => '_profiler'));
            }

        }

        if (0 === strpos($pathinfo, '/_configurator')) {
            // _configurator_home
            if (rtrim($pathinfo, '/') === '/_configurator') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_configurator_home');
                }
                return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
            }

            // _configurator_step
            if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]+?)$#xs', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',)), array('_route' => '_configurator_step'));
            }

            // _configurator_final
            if ($pathinfo === '/_configurator/final') {
                return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
            }

        }

        // SwisscomMatrixConfigBundle_homepage
        if (rtrim($pathinfo, '/') === '/Matrix/ConfigGen') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'SwisscomMatrixConfigBundle_homepage');
            }
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\DefaultController::indexAction',  '_route' => 'SwisscomMatrixConfigBundle_homepage',);
        }

        // SwisscomMatrixConfigBundle_project
        if (rtrim($pathinfo, '/') === '/Matrix/ConfigGen/project') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'SwisscomMatrixConfigBundle_project');
            }
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::indexAction',  '_route' => 'SwisscomMatrixConfigBundle_project',);
        }

        // SwisscomMatrixConfigBundle_project_new
        if ($pathinfo === '/Matrix/ConfigGen/project/new') {
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::newAction',  '_route' => 'SwisscomMatrixConfigBundle_project_new',);
        }

        // SwisscomMatrixConfigBundle_project_create
        if ($pathinfo === '/Matrix/ConfigGen/project/create') {
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::createAction',  '_route' => 'SwisscomMatrixConfigBundle_project_create',);
        }

        // SwisscomMatrixConfigBundle_project_edit
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/project') && preg_match('#^/Matrix/ConfigGen/project/(?P<id>[^/]+?)/edit$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::editAction',)), array('_route' => 'SwisscomMatrixConfigBundle_project_edit'));
        }

        // SwisscomMatrixConfigBundle_project_update
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/project') && preg_match('#^/Matrix/ConfigGen/project/(?P<id>[^/]+?)/update$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::updateAction',)), array('_route' => 'SwisscomMatrixConfigBundle_project_update'));
        }

        // SwisscomMatrixConfigBundle_project_show
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/project') && preg_match('#^/Matrix/ConfigGen/project/(?P<id>[^/]+?)/show$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::showAction',)), array('_route' => 'SwisscomMatrixConfigBundle_project_show'));
        }

        // SwisscomMatrixConfigBundle_project_delete
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/project') && preg_match('#^/Matrix/ConfigGen/project/(?P<id>[^/]+?)/delete$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\projectController::deleteAction',)), array('_route' => 'SwisscomMatrixConfigBundle_project_delete'));
        }

        // SwisscomMatrixConfigBundle_connection_type
        if (rtrim($pathinfo, '/') === '/Matrix/ConfigGen/connection/type') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'SwisscomMatrixConfigBundle_connection_type');
            }
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::indexAction',  '_route' => 'SwisscomMatrixConfigBundle_connection_type',);
        }

        // SwisscomMatrixConfigBundle_connection_type_new
        if ($pathinfo === '/Matrix/ConfigGen/connection/type/new') {
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::newAction',  '_route' => 'SwisscomMatrixConfigBundle_connection_type_new',);
        }

        // SwisscomMatrixConfigBundle_connection_type_create
        if ($pathinfo === '/Matrix/ConfigGen/connection/type/create') {
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::createAction',  '_route' => 'SwisscomMatrixConfigBundle_connection_type_create',);
        }

        // SwisscomMatrixConfigBundle_connection_type_edit
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/connection/type') && preg_match('#^/Matrix/ConfigGen/connection/type/(?P<id>[^/]+?)/edit$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::editAction',)), array('_route' => 'SwisscomMatrixConfigBundle_connection_type_edit'));
        }

        // SwisscomMatrixConfigBundle_connection_type_update
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/connection/type') && preg_match('#^/Matrix/ConfigGen/connection/type/(?P<id>[^/]+?)/update$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::updateAction',)), array('_route' => 'SwisscomMatrixConfigBundle_connection_type_update'));
        }

        // SwisscomMatrixConfigBundle_connection_type_show
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/connection/type') && preg_match('#^/Matrix/ConfigGen/connection/type/(?P<id>[^/]+?)/show$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::showAction',)), array('_route' => 'SwisscomMatrixConfigBundle_connection_type_show'));
        }

        // SwisscomMatrixConfigBundle_connection_type_delete
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/connection/type') && preg_match('#^/Matrix/ConfigGen/connection/type/(?P<id>[^/]+?)/delete$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\connection_typeController::deleteAction',)), array('_route' => 'SwisscomMatrixConfigBundle_connection_type_delete'));
        }

        // SwisscomMatrixConfigBundle_config_store
        if (rtrim($pathinfo, '/') === '/Matrix/ConfigGen/config/store') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'SwisscomMatrixConfigBundle_config_store');
            }
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::indexAction',  '_route' => 'SwisscomMatrixConfigBundle_config_store',);
        }

        // SwisscomMatrixConfigBundle_config_store_new
        if ($pathinfo === '/Matrix/ConfigGen/config/store/new') {
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::newAction',  '_route' => 'SwisscomMatrixConfigBundle_config_store_new',);
        }

        // SwisscomMatrixConfigBundle_config_store_create
        if ($pathinfo === '/Matrix/ConfigGen/config/store/create') {
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::createAction',  '_route' => 'SwisscomMatrixConfigBundle_config_store_create',);
        }

        // SwisscomMatrixConfigBundle_config_store_edit
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/config/store') && preg_match('#^/Matrix/ConfigGen/config/store/(?P<id>[^/]+?)/edit$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::editAction',)), array('_route' => 'SwisscomMatrixConfigBundle_config_store_edit'));
        }

        // SwisscomMatrixConfigBundle_config_store_update
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/config/store') && preg_match('#^/Matrix/ConfigGen/config/store/(?P<id>[^/]+?)/update$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::updateAction',)), array('_route' => 'SwisscomMatrixConfigBundle_config_store_update'));
        }

        // SwisscomMatrixConfigBundle_config_store_show
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/config/store') && preg_match('#^/Matrix/ConfigGen/config/store/(?P<id>[^/]+?)/show$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::showAction',)), array('_route' => 'SwisscomMatrixConfigBundle_config_store_show'));
        }

        // SwisscomMatrixConfigBundle_config_store_delete
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/config/store') && preg_match('#^/Matrix/ConfigGen/config/store/(?P<id>[^/]+?)/delete$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_storeController::deleteAction',)), array('_route' => 'SwisscomMatrixConfigBundle_config_store_delete'));
        }

        // SwisscomMatrixConfigBundle_config_template
        if (rtrim($pathinfo, '/') === '/Matrix/ConfigGen/config/template') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'SwisscomMatrixConfigBundle_config_template');
            }
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::indexAction',  '_route' => 'SwisscomMatrixConfigBundle_config_template',);
        }

        // SwisscomMatrixConfigBundle_config_template_new
        if ($pathinfo === '/Matrix/ConfigGen/config/template/new') {
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::newAction',  '_route' => 'SwisscomMatrixConfigBundle_config_template_new',);
        }

        // SwisscomMatrixConfigBundle_config_template_create
        if ($pathinfo === '/Matrix/ConfigGen/config/template/create') {
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::createAction',  '_route' => 'SwisscomMatrixConfigBundle_config_template_create',);
        }

        // SwisscomMatrixConfigBundle_config_template_edit
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/config/template') && preg_match('#^/Matrix/ConfigGen/config/template/(?P<id>[^/]+?)/edit$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::editAction',)), array('_route' => 'SwisscomMatrixConfigBundle_config_template_edit'));
        }

        // SwisscomMatrixConfigBundle_config_template_update
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/config/template') && preg_match('#^/Matrix/ConfigGen/config/template/(?P<id>[^/]+?)/update$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::updateAction',)), array('_route' => 'SwisscomMatrixConfigBundle_config_template_update'));
        }

        // SwisscomMatrixConfigBundle_config_template_show
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/config/template') && preg_match('#^/Matrix/ConfigGen/config/template/(?P<id>[^/]+?)/show$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::showAction',)), array('_route' => 'SwisscomMatrixConfigBundle_config_template_show'));
        }

        // SwisscomMatrixConfigBundle_config_template_delete
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/config/template') && preg_match('#^/Matrix/ConfigGen/config/template/(?P<id>[^/]+?)/delete$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\config_templateController::deleteAction',)), array('_route' => 'SwisscomMatrixConfigBundle_config_template_delete'));
        }

        // SwisscomMatrixConfigBundle_parameters_form
        if (rtrim($pathinfo, '/') === '/Matrix/ConfigGen/parameters/form') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'SwisscomMatrixConfigBundle_parameters_form');
            }
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::indexAction',  '_route' => 'SwisscomMatrixConfigBundle_parameters_form',);
        }

        // SwisscomMatrixConfigBundle_parameters_form_new
        if ($pathinfo === '/Matrix/ConfigGen/parameters/form/new') {
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::newAction',  '_route' => 'SwisscomMatrixConfigBundle_parameters_form_new',);
        }

        // SwisscomMatrixConfigBundle_parameters_form_create
        if ($pathinfo === '/Matrix/ConfigGen/parameters/form/create') {
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::createAction',  '_route' => 'SwisscomMatrixConfigBundle_parameters_form_create',);
        }

        // SwisscomMatrixConfigBundle_parameters_form_edit
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/parameters/form') && preg_match('#^/Matrix/ConfigGen/parameters/form/(?P<id>[^/]+?)/edit$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::editAction',)), array('_route' => 'SwisscomMatrixConfigBundle_parameters_form_edit'));
        }

        // SwisscomMatrixConfigBundle_parameters_form_update
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/parameters/form') && preg_match('#^/Matrix/ConfigGen/parameters/form/(?P<id>[^/]+?)/update$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::updateAction',)), array('_route' => 'SwisscomMatrixConfigBundle_parameters_form_update'));
        }

        // SwisscomMatrixConfigBundle_parameters_form_show
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/parameters/form') && preg_match('#^/Matrix/ConfigGen/parameters/form/(?P<id>[^/]+?)/show$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::showAction',)), array('_route' => 'SwisscomMatrixConfigBundle_parameters_form_show'));
        }

        // SwisscomMatrixConfigBundle_parameters_form_delete
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/parameters/form') && preg_match('#^/Matrix/ConfigGen/parameters/form/(?P<id>[^/]+?)/delete$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\parameters_formController::deleteAction',)), array('_route' => 'SwisscomMatrixConfigBundle_parameters_form_delete'));
        }

        // SwisscomMatrixConfigBundle_loopback
        if (rtrim($pathinfo, '/') === '/Matrix/ConfigGen/loopback') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'SwisscomMatrixConfigBundle_loopback');
            }
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::indexAction',  '_route' => 'SwisscomMatrixConfigBundle_loopback',);
        }

        // SwisscomMatrixConfigBundle_loopback_new
        if ($pathinfo === '/Matrix/ConfigGen/loopback/new') {
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::newAction',  '_route' => 'SwisscomMatrixConfigBundle_loopback_new',);
        }

        // SwisscomMatrixConfigBundle_loopback_create
        if ($pathinfo === '/Matrix/ConfigGen/loopback/create') {
            return array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::createAction',  '_route' => 'SwisscomMatrixConfigBundle_loopback_create',);
        }

        // SwisscomMatrixConfigBundle_loopback_edit
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/loopback') && preg_match('#^/Matrix/ConfigGen/loopback/(?P<id>[^/]+?)/edit$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::editAction',)), array('_route' => 'SwisscomMatrixConfigBundle_loopback_edit'));
        }

        // SwisscomMatrixConfigBundle_loopback_update
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/loopback') && preg_match('#^/Matrix/ConfigGen/loopback/(?P<id>[^/]+?)/update$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::updateAction',)), array('_route' => 'SwisscomMatrixConfigBundle_loopback_update'));
        }

        // SwisscomMatrixConfigBundle_loopback_show
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/loopback') && preg_match('#^/Matrix/ConfigGen/loopback/(?P<id>[^/]+?)/show$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::showAction',)), array('_route' => 'SwisscomMatrixConfigBundle_loopback_show'));
        }

        // SwisscomMatrixConfigBundle_loopback_delete
        if (0 === strpos($pathinfo, '/Matrix/ConfigGen/loopback') && preg_match('#^/Matrix/ConfigGen/loopback/(?P<id>[^/]+?)/delete$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Swisscom\\Matrix\\ConfigBundle\\Controller\\loopbackController::deleteAction',)), array('_route' => 'SwisscomMatrixConfigBundle_loopback_delete'));
        }

        // SwisscomMatrixHomeBundle_homepage
        if ($pathinfo === '/Matrix/Hello') {
            return array (  '_controller' => 'Swisscom\\Matrix\\HomeBundle\\Controller\\DefaultController::helloAction',  '_route' => 'SwisscomMatrixHomeBundle_homepage',);
        }

        // AcmeHelloBundle_homepage
        if (0 === strpos($pathinfo, '/hello') && preg_match('#^/hello/(?P<name>[^/]+?)$#xs', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Acme\\HelloBundle\\Controller\\DefaultController::indexAction',)), array('_route' => 'AcmeHelloBundle_homepage'));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
