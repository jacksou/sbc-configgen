<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();
/*
 * Home Page routing
 * 
 */

$collection->add('SwisscomMatrixConfigBundle_homepage', new Route('/Matrix/ConfigGen/', array(
    '_controller' => 'SwisscomMatrixConfigBundle:Default:index',
)));


/*
 * 
 * Project Routing
 * 
 */
//Index Project
$collection->add('SwisscomMatrixConfigBundle_project', new Route('/Matrix/ConfigGen/project/', array(
    '_controller' => 'SwisscomMatrixConfigBundle:project:index',
)));

//New project 
$collection->add('SwisscomMatrixConfigBundle_project_new', new Route('/Matrix/ConfigGen/project/new', array(
    '_controller' => 'SwisscomMatrixConfigBundle:project:new',
)));

//Create project 
$collection->add('SwisscomMatrixConfigBundle_project_create', new Route('/Matrix/ConfigGen/project/create', array(
    '_controller' => 'SwisscomMatrixConfigBundle:project:create',
)));

//Edit project 
$collection->add('SwisscomMatrixConfigBundle_project_edit', new Route('/Matrix/ConfigGen/project/{id}/edit', array(
    '_controller' => 'SwisscomMatrixConfigBundle:project:edit',
)));

//Update project 
$collection->add('SwisscomMatrixConfigBundle_project_update', new Route('/Matrix/ConfigGen/project/{id}/update', array(
    '_controller' => 'SwisscomMatrixConfigBundle:project:update',
)));

//Show project 
$collection->add('SwisscomMatrixConfigBundle_project_show', new Route('/Matrix/ConfigGen/project/{id}/show', array(
    '_controller' => 'SwisscomMatrixConfigBundle:project:show',
)));

//Delete project 
$collection->add('SwisscomMatrixConfigBundle_project_delete', new Route('/Matrix/ConfigGen/project/{id}/delete', array(
    '_controller' => 'SwisscomMatrixConfigBundle:project:delete',
)));


/*
 * 
 * Connection_Type Routing
 * 
 */
//Index Project
$collection->add('SwisscomMatrixConfigBundle_connection_type', new Route('/Matrix/ConfigGen/connection/type/', array(
    '_controller' => 'SwisscomMatrixConfigBundle:connection_type:index',
)));

//New project 
$collection->add('SwisscomMatrixConfigBundle_connection_type_new', new Route('/Matrix/ConfigGen/connection/type/new', array(
    '_controller' => 'SwisscomMatrixConfigBundle:connection_type:new',
)));

//Create project 
$collection->add('SwisscomMatrixConfigBundle_connection_type_create', new Route('/Matrix/ConfigGen/connection/type/create', array(
    '_controller' => 'SwisscomMatrixConfigBundle:connection_type:create',
)));

//Edit project 
$collection->add('SwisscomMatrixConfigBundle_connection_type_edit', new Route('/Matrix/ConfigGen/connection/type/{id}/edit', array(
    '_controller' => 'SwisscomMatrixConfigBundle:connection_type:edit',
)));

//Update project 
$collection->add('SwisscomMatrixConfigBundle_connection_type_update', new Route('/Matrix/ConfigGen/connection/type/{id}/update', array(
    '_controller' => 'SwisscomMatrixConfigBundle:connection_type:update',
)));

//Show project 
$collection->add('SwisscomMatrixConfigBundle_connection_type_show', new Route('/Matrix/ConfigGen/connection/type/{id}/show', array(
    '_controller' => 'SwisscomMatrixConfigBundle:connection_type:show',
)));

//Delete project 
$collection->add('SwisscomMatrixConfigBundle_connection_type_delete', new Route('/Matrix/ConfigGen/connection/type/{id}/delete', array(
    '_controller' => 'SwisscomMatrixConfigBundle:connection_type:delete',
)));


/*
 * 
 * Config_store Routing
 * 
 */
//Index Project
$collection->add('SwisscomMatrixConfigBundle_config_store', new Route('/Matrix/ConfigGen/config/store/', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_store:index',
)));

//New project 
$collection->add('SwisscomMatrixConfigBundle_config_store_new', new Route('/Matrix/ConfigGen/config/store/new', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_store:new',
)));

//Create project 
$collection->add('SwisscomMatrixConfigBundle_config_store_create', new Route('/Matrix/ConfigGen/config/store/create', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_store:create',
)));

//Edit project 
$collection->add('SwisscomMatrixConfigBundle_config_store_edit', new Route('/Matrix/ConfigGen/config/store/{id}/edit', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_store:edit',
)));

//Update project 
$collection->add('SwisscomMatrixConfigBundle_config_store_update', new Route('/Matrix/ConfigGen/config/store/{id}/update', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_store:update',
)));

//Show project 
$collection->add('SwisscomMatrixConfigBundle_config_store_show', new Route('/Matrix/ConfigGen/config/store/{id}/show', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_store:show',
)));

//Delete project 
$collection->add('SwisscomMatrixConfigBundle_config_store_delete', new Route('/Matrix/ConfigGen/config/store/{id}/delete', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_store:delete',
)));



/*
 * 
 * Config_template Routing
 * 
 */
//Index Project
$collection->add('SwisscomMatrixConfigBundle_config_template', new Route('/Matrix/ConfigGen/config/template/', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_template:index',
)));

//New project 
$collection->add('SwisscomMatrixConfigBundle_config_template_new', new Route('/Matrix/ConfigGen/config/template/new', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_template:new',
)));

//Create project 
$collection->add('SwisscomMatrixConfigBundle_config_template_create', new Route('/Matrix/ConfigGen/config/template/create', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_template:create',
)));

//Edit project 
$collection->add('SwisscomMatrixConfigBundle_config_template_edit', new Route('/Matrix/ConfigGen/config/template/{id}/edit', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_template:edit',
)));

//Update project 
$collection->add('SwisscomMatrixConfigBundle_config_template_update', new Route('/Matrix/ConfigGen/config/template/{id}/update', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_template:update',
)));

//Show project 
$collection->add('SwisscomMatrixConfigBundle_config_template_show', new Route('/Matrix/ConfigGen/config/template/{id}/show', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_template:show',
)));

//Delete project 
$collection->add('SwisscomMatrixConfigBundle_config_template_delete', new Route('/Matrix/ConfigGen/config/template/{id}/delete', array(
    '_controller' => 'SwisscomMatrixConfigBundle:config_template:delete',
)));


/*
 * 
 * Parameters_form Routing
 * 
 */
//Index Project
$collection->add('SwisscomMatrixConfigBundle_parameters_form', new Route('/Matrix/ConfigGen/parameters/form/', array(
    '_controller' => 'SwisscomMatrixConfigBundle:parameters_form:index',
)));

//New project 
$collection->add('SwisscomMatrixConfigBundle_parameters_form_new', new Route('/Matrix/ConfigGen/parameters/form/new', array(
    '_controller' => 'SwisscomMatrixConfigBundle:parameters_form:new',
)));

//Create project 
$collection->add('SwisscomMatrixConfigBundle_parameters_form_create', new Route('/Matrix/ConfigGen/parameters/form/create', array(
    '_controller' => 'SwisscomMatrixConfigBundle:parameters_form:create',
)));

//Edit project 
$collection->add('SwisscomMatrixConfigBundle_parameters_form_edit', new Route('/Matrix/ConfigGen/parameters/form/{id}/edit', array(
    '_controller' => 'SwisscomMatrixConfigBundle:parameters_form:edit',
)));

//Update project 
$collection->add('SwisscomMatrixConfigBundle_parameters_form_update', new Route('/Matrix/ConfigGen/parameters/form/{id}/update', array(
    '_controller' => 'SwisscomMatrixConfigBundle:parameters_form:update',
)));

//Show project 
$collection->add('SwisscomMatrixConfigBundle_parameters_form_show', new Route('/Matrix/ConfigGen/parameters/form/{id}/show', array(
    '_controller' => 'SwisscomMatrixConfigBundle:parameters_form:show',
)));

//Delete project 
$collection->add('SwisscomMatrixConfigBundle_parameters_form_delete', new Route('/Matrix/ConfigGen/parameters/form/{id}/delete', array(
    '_controller' => 'SwisscomMatrixConfigBundle:parameters_form:delete',
)));


/*
 * 
 * loopback Routing
 * 
 */
//Index Project
$collection->add('SwisscomMatrixConfigBundle_loopback', new Route('/Matrix/ConfigGen/loopback/', array(
    '_controller' => 'SwisscomMatrixConfigBundle:loopback:index',
)));

//New project 
$collection->add('SwisscomMatrixConfigBundle_loopback_new', new Route('/Matrix/ConfigGen/loopback/new', array(
    '_controller' => 'SwisscomMatrixConfigBundle:loopback:new',
)));

//Create project 
$collection->add('SwisscomMatrixConfigBundle_loopback_create', new Route('/Matrix/ConfigGen/loopback/create', array(
    '_controller' => 'SwisscomMatrixConfigBundle:loopback:create',
)));

//Edit project 
$collection->add('SwisscomMatrixConfigBundle_loopback_edit', new Route('/Matrix/ConfigGen/loopback/{id}/edit', array(
    '_controller' => 'SwisscomMatrixConfigBundle:loopback:edit',
)));

//Update project 
$collection->add('SwisscomMatrixConfigBundle_loopback_update', new Route('/Matrix/ConfigGen/loopback/{id}/update', array(
    '_controller' => 'SwisscomMatrixConfigBundle:loopback:update',
)));

//Show project 
$collection->add('SwisscomMatrixConfigBundle_loopback_show', new Route('/Matrix/ConfigGen/loopback/{id}/show', array(
    '_controller' => 'SwisscomMatrixConfigBundle:loopback:show',
)));

//Delete project 
$collection->add('SwisscomMatrixConfigBundle_loopback_delete', new Route('/Matrix/ConfigGen/loopback/{id}/delete', array(
    '_controller' => 'SwisscomMatrixConfigBundle:loopback:delete',
)));


return $collection;
