<?php

namespace Swisscom\Matrix\ConfigBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Swisscom\Matrix\ConfigBundle\Form\projectType;
use Swisscom\Matrix\ConfigBundle\Entity\project;


class DefaultController extends Controller
{
    /**
     * @Route("/hello/{name}")
     * @Template()
     */
    public function indexAction($name = 'hh')
    {
	        return $this->render('SwisscomMatrixConfigBundle:Default:index.html.twig', array('name' => $name));
    }
    
   
}
