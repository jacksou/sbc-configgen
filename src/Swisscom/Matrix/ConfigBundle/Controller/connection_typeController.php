<?php

namespace Swisscom\Matrix\ConfigBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Swisscom\Matrix\ConfigBundle\Entity\connection_type;
use Swisscom\Matrix\ConfigBundle\Form\connection_typeType;

/**
 * connection_type controller.
 *
 * @Route("/SwisscomMatrixConfigBundle_connection_type")
 */
class connection_typeController extends Controller
{
    /**
     * Lists all connection_type entities.
     *
     * @Route("/", name="SwisscomMatrixConfigBundle_connection_type")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('SwisscomMatrixConfigBundle:connection_type')->findAll();

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a connection_type entity.
     *
     * @Route("/{id}/show", name="SwisscomMatrixConfigBundle_connection_type_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('SwisscomMatrixConfigBundle:connection_type')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find connection_type entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        );
    }

    /**
     * Displays a form to create a new connection_type entity.
     *
     * @Route("/new", name="SwisscomMatrixConfigBundle_connection_type_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new connection_type();
        $form   = $this->createForm(new connection_typeType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new connection_type entity.
     *
     * @Route("/create", name="SwisscomMatrixConfigBundle_connection_type_create")
     * @Method("post")
     * @Template("SwisscomMatrixConfigBundle:connection_type:new.html.twig")
     */
    public function createAction()
    {
        $entity  = new connection_type();
        $request = $this->getRequest();
        $form    = $this->createForm(new connection_typeType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('SwisscomMatrixConfigBundle_connection_type_show', array('id' => $entity->getId())));
            
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing connection_type entity.
     *
     * @Route("/{id}/edit", name="SwisscomMatrixConfigBundle_connection_type_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('SwisscomMatrixConfigBundle:connection_type')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find connection_type entity.');
        }

        $editForm = $this->createForm(new connection_typeType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing connection_type entity.
     *
     * @Route("/{id}/update", name="SwisscomMatrixConfigBundle_connection_type_update")
     * @Method("post")
     * @Template("SwisscomMatrixConfigBundle:connection_type:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('SwisscomMatrixConfigBundle:connection_type')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find connection_type entity.');
        }

        $editForm   = $this->createForm(new connection_typeType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('SwisscomMatrixConfigBundle_connection_type_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a connection_type entity.
     *
     * @Route("/{id}/delete", name="SwisscomMatrixConfigBundle_connection_type_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('SwisscomMatrixConfigBundle:connection_type')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find connection_type entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('SwisscomMatrixConfigBundle_connection_type'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
