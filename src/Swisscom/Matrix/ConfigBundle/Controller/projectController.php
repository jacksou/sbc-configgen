<?php

namespace Swisscom\Matrix\ConfigBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Swisscom\Matrix\ConfigBundle\Entity\project;
use Swisscom\Matrix\ConfigBundle\Form\projectType;

/**
 * project controller.
 *
 * @Route("/SwisscomMatrixConfigBundle_project")
 */
class projectController extends Controller
{
    /**
     * Lists all project entities.
     *
     * @Route("/", name="SwisscomMatrixConfigBundle_project")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('SwisscomMatrixConfigBundle:project')->findAll();

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a project entity.
     *
     * @Route("/{id}/show", name="SwisscomMatrixConfigBundle_project_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('SwisscomMatrixConfigBundle:project')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find project entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        );
    }

    /**
     * Displays a form to create a new project entity.
     *
     * @Route("/new", name="SwisscomMatrixConfigBundle_project_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new project();
        $form   = $this->createForm(new projectType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new project entity.
     *
     * @Route("/create", name="SwisscomMatrixConfigBundle_project_create")
     * @Method("post")
     * @Template("SwisscomMatrixConfigBundle:project:new.html.twig")
     */
    public function createAction()
    {
        $entity  = new project();
        $request = $this->getRequest();
        $form    = $this->createForm(new projectType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('SwisscomMatrixConfigBundle_project_show', array('id' => $entity->getId())));
            
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing project entity.
     *
     * @Route("/{id}/edit", name="SwisscomMatrixConfigBundle_project_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('SwisscomMatrixConfigBundle:project')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find project entity.');
        }

        $editForm = $this->createForm(new projectType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing project entity.
     *
     * @Route("/{id}/update", name="SwisscomMatrixConfigBundle_project_update")
     * @Method("post")
     * @Template("SwisscomMatrixConfigBundle:project:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('SwisscomMatrixConfigBundle:project')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find project entity.');
        }

        $editForm   = $this->createForm(new projectType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('SwisscomMatrixConfigBundle_project_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a project entity.
     *
     * @Route("/{id}/delete", name="SwisscomMatrixConfigBundle_project_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('SwisscomMatrixConfigBundle:project')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find project entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('SwisscomMatrixConfigBundle_project'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
