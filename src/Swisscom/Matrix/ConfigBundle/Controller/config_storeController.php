<?php

namespace Swisscom\Matrix\ConfigBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Swisscom\Matrix\ConfigBundle\Entity\config_store;
use Swisscom\Matrix\ConfigBundle\Form\config_storeType;

/**
 * config_store controller.
 *
 * @Route("/SwisscomMatrixConfigBundle_config_store")
 */
class config_storeController extends Controller
{
    /**
     * Lists all config_store entities.
     *
     * @Route("/", name="SwisscomMatrixConfigBundle_config_store")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('SwisscomMatrixConfigBundle:config_store')->findAll();

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a config_store entity.
     *
     * @Route("/{id}/show", name="SwisscomMatrixConfigBundle_config_store_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('SwisscomMatrixConfigBundle:config_store')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find config_store entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        );
    }

    /**
     * Displays a form to create a new config_store entity.
     *
     * @Route("/new", name="SwisscomMatrixConfigBundle_config_store_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new config_store();
        $form   = $this->createForm(new config_storeType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new config_store entity.
     *
     * @Route("/create", name="SwisscomMatrixConfigBundle_config_store_create")
     * @Method("post")
     * @Template("SwisscomMatrixConfigBundle:config_store:new.html.twig")
     */
    public function createAction()
    {
        $entity  = new config_store();
        $request = $this->getRequest();
        $form    = $this->createForm(new config_storeType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('SwisscomMatrixConfigBundle_config_store_show', array('id' => $entity->getId())));
            
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing config_store entity.
     *
     * @Route("/{id}/edit", name="SwisscomMatrixConfigBundle_config_store_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('SwisscomMatrixConfigBundle:config_store')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find config_store entity.');
        }

        $editForm = $this->createForm(new config_storeType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing config_store entity.
     *
     * @Route("/{id}/update", name="SwisscomMatrixConfigBundle_config_store_update")
     * @Method("post")
     * @Template("SwisscomMatrixConfigBundle:config_store:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('SwisscomMatrixConfigBundle:config_store')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find config_store entity.');
        }

        $editForm   = $this->createForm(new config_storeType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('SwisscomMatrixConfigBundle_config_store_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a config_store entity.
     *
     * @Route("/{id}/delete", name="SwisscomMatrixConfigBundle_config_store_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('SwisscomMatrixConfigBundle:config_store')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find config_store entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('SwisscomMatrixConfigBundle_config_store'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
