<?php

namespace Swisscom\Matrix\ConfigBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class config_storeType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('connection_type')
            ->add('config_template')
            ->add('loopback')
        ;
    }

    public function getName()
    {
        return 'swisscom_matrix_configbundle_config_storetype';
    }
}
