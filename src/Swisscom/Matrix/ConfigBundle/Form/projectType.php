<?php

namespace Swisscom\Matrix\ConfigBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class projectType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('project_code')
            ->add('name')
        ;
    }

    public function getName()
    {
        return 'swisscom_matrix_configbundle_projecttype';
    }
}
