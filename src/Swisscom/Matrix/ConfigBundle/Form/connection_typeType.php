<?php

namespace Swisscom\Matrix\ConfigBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class connection_typeType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('image')
            ->add('remark')
            ->add('project')
        ;
    }

    public function getName()
    {
        return 'swisscom_matrix_configbundle_connection_typetype';
    }
}
