<?php

namespace Swisscom\Matrix\ConfigBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Swisscom\Matrix\ConfigBundle\Entity\parameters_form
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Swisscom\Matrix\ConfigBundle\Entity\parameters_formRepository")
 */
class parameters_form
{
 /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string $value
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;
    
 	/**
     *  @ORM\ManyToOne(targetEntity="config_store", inversedBy="parameters_form")
     *  @ORM\JoinColumn(name="config_store_id", referencedColumnName="id")
     */
    protected $configs_store;


	  
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set value
     *
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }



    /**
     * Get configs
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getConfigs()
    {
        return $this->configs;
    }
    
            public function __toString() { return $this->name; }
    
    
}