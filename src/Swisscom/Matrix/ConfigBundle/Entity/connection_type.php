<?php

namespace Swisscom\Matrix\ConfigBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Swisscom\Matrix\ConfigBundle\Entity\connection_type
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Swisscom\Matrix\ConfigBundle\Entity\connection_typeRepository")
 */
class connection_type
{
/**
	 * @var integer $id
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
     * @ORM\ManyToOne(targetEntity="project", inversedBy="connection_type")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    protected $project;
    
    /**
	 * @ORM\OneToMany(targetEntity="config_store", mappedBy="connection_type")
	 */
	protected $config_store;
	
	
	/**
	 * @var string $name
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;
	
	/**
	 * @var text $description
	 *
	 * @ORM\Column(name="description", type="text")
	 */
	private $description;
	
	/**
	 * @var string $image
	 *
	 * @ORM\Column(name="image", type="string", length=255)
	 */
	private $image;
	
	/**
	 * @var text $remark
	 *
	 * @ORM\Column(name="remark", type="text")
	 */
	private $remark;
	
	
	public function __construct() {
		$this->config_store = new ArrayCollection();
	}
	
	
	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Set name
	 *
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
	}
	
	/**
	 * Get name
	 *
	 * @return string 
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Set description
	 *
	 * @param text $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}
	
	/**
	 * Get description
	 *
	 * @return text 
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * Set image
	 *
	 * @param string $image
	 */
	public function setImage($image) {
		$this->image = $image;
	}
	
	/**
	 * Get image
	 *
	 * @return string 
	 */
	public function getImage() {
		return $this->image;
	}
	
	/**
	 * Set remark
	 *
	 * @param text $remark
	 */
	public function setRemark($remark) {
		$this->remark = $remark;
	}
	
	/**
	 * Get remark
	 *
	 * @return text 
	 */
	public function getRemark() {
		return $this->remark;
	}

    /**
     * Set project
     *
     * @param Swisscom\Matrix\StoreBundle\Entity\project $project
     */
    public function setProject($project)
    {
        $this->project = $project;
    }

    /**
     * Get project
     *
     * @return Swisscom\Matrix\StoreBundle\Entity\project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Add config_store
     *
     * @param Swisscom\Matrix\StoreBundle\Entity\config_store $configStore
     */
    public function addconfig_store(\Swisscom\Matrix\StoreBundle\Entity\config_store $configStore)
    {
        $this->config_store[] = $configStore;
    }

    /**
     * Get config_store
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getConfigStore()
    {
        return $this->config_store;
    }
        public function __toString() { return $this->project.':'.$this->name; }
    
}