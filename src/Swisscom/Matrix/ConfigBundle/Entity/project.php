<?php

namespace Swisscom\Matrix\ConfigBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Swisscom\Matrix\ConfigBundle\Entity\project
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Swisscom\Matrix\ConfigBundle\Entity\projectRepository")
 */
class project
{
    /**
	 * @ORM\OneToMany(targetEntity="connection_type", mappedBy="Project")
	 */
	protected $connection_type;
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int $project_code
     *
     * @ORM\Column(name="project_code", type="integer", length=255)
     */
    private $project_code;

    /**
     * @var text $name
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

	public function __construct() {
		$this->connection_type = new ArrayCollection();
	}
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set project_code
     *
     * @param int $pkProject
     */
    public function setProjectCode( $pkProject)
    {
        $this->project_code = $pkProject;
    }

    /**
     * Get project_code
     *
     * @return int 
     */
    public function getProjectCode()
    {
        return $this->project_code;
    }

    /**
     * Set name
     *
     * @param text $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return text 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add connection_type
     *
     * @param Swisscom\Matrix\StoreBundle\Entity\connection_type $connectionType
     */
    public function addconnection_type(\Swisscom\Matrix\StoreBundle\Entity\connection_type $connectionType)
    {
        $this->connection_type[] = $connectionType;
    }

    /**
     * Get connection_type
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getConnectionType()
    {
        return $this->connection_type;
    }
    
    public function __toString() { return $this->name; }
}