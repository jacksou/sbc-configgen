<?php

namespace Swisscom\Matrix\ConfigBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Swisscom\Matrix\ConfigBundle\Entity\config_template
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Swisscom\Matrix\ConfigBundle\Entity\config_templateRepository")
 */
class config_template {
	/**
	 * @var integer $id
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @ORM\OneToMany(targetEntity="config_store", mappedBy="config_template")
	 */
	protected $config_store;
	
	/**
	 * @var string $name
	 *
	 * @ORM\Column(name="name", type="string", length=255)
	 */
	private $name;
	
	/**
	 * @var text $description
	 *
	 * @ORM\Column(name="description", type="text")
	 */
	private $description;
	
	/**
	 * @var text $template
	 *
	 * @ORM\Column(name="template", type="text")
	 */
	private $template;
	
	public function __construct() {
		$this->config_store = new ArrayCollection ();
	}
	
	/**
	 * Get id
	 *
	 * @return integer 
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Set name
	 *
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $name;
	}
	
	/**
	 * Get name
	 *
	 * @return string 
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Set description
	 *
	 * @param text $description
	 */
	public function setDescription($description) {
		$this->description = $description;
	}
	
	/**
	 * Get description
	 *
	 * @return text 
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * Set template
	 *
	 * @param text $template
	 */
	public function setTemplate($template) {
		$this->template = $template;
	}
	
	/**
	 * Get template
	 *
	 * @return text 
	 */
	public function getTemplate() {
		return $this->template;
	}
	
	/**
	 * Add config_store
	 *
	 * @param Swisscom\Matrix\StoreBundle\Entity\config_store $configStore
	 */
	public function addconfig_store(Swisscom\Matrix\StoreBundle\Entity\config_store $configStore) {
		$this->config_store [] = $configStore;
	}
	
	/**
	 * Get config_store
	 *
	 * @return Doctrine\Common\Collections\Collection 
	 */
	public function getConfigStore() {
		return $this->config_store;
	}
	
	public function __toString() {
		return $this->name;
	}

}