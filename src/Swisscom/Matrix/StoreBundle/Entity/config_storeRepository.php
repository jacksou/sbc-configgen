<?php

namespace Swisscom\Matrix\StoreBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * config_storeRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class config_storeRepository extends EntityRepository
{
}