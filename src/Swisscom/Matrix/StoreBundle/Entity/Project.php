<?php

namespace Swisscom\Matrix\StoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Swisscom\Matrix\StoreBundle\Entity\Project
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Swisscom\Matrix\StoreBundle\Entity\ProjectRepository")
 */
class Project
{
	
	/**
	 * @ORM\OneToMany(targetEntity="connection_type", mappedBy="Project")
	 */
	protected $connection_type;
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int $pk_project
     *
     * @ORM\Column(name="pk_project", type="integer", length=255)
     */
    private $pk_project;

    /**
     * @var text $name
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

	public function __construct() {
		$this->connection_type = new Collections\ArrayCollection();
	}
	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pk_project
     *
     * @param int $pkProject
     */
    public function setPkProject(\int $pkProject)
    {
        $this->pk_project = $pkProject;
    }

    /**
     * Get pk_project
     *
     * @return int 
     */
    public function getPkProject()
    {
        return $this->pk_project;
    }

    /**
     * Set name
     *
     * @param text $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return text 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add connection_type
     *
     * @param Swisscom\Matrix\StoreBundle\Entity\connection_type $connectionType
     */
    public function addconnection_type(\Swisscom\Matrix\StoreBundle\Entity\connection_type $connectionType)
    {
        $this->connection_type[] = $connectionType;
    }

    /**
     * Get connection_type
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getConnectionType()
    {
        return $this->connection_type;
    }
}