<?php

namespace Swisscom\Matrix\StoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Swisscom\Matrix\StoreBundle\Entity\config_store
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Swisscom\Matrix\StoreBundle\Entity\config_storeRepository")
 */
class config_store
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
	/**
     * @ORM\ManyToOne(targetEntity="connection_type", inversedBy="config_store")
     * @ORM\JoinColumn(name="connection_type_id", referencedColumnName="id")
     */
    protected $connection_type;
    
    	
    /**
     * @ORM\ManyToOne(targetEntity="config_template", inversedBy="config_store")
     * @ORM\JoinColumn(name="config_template_id", referencedColumnName="id")
     */
    protected $config_template;
    
     /**
     * @ORM\ManyToOne(targetEntity="loopback", inversedBy="config_store")
     * @ORM\JoinColumn(name="loopback_id", referencedColumnName="id")
     */
    protected $loopback;
    
    
    /**
     *  @ORM\ManyToMany(targetEntity="parameters_form", inversedBy="configs")
     *  @ORM\JoinTable(name="configs_params")
     */
    protected $params;
    
    
	public function __construct() {
        $this->params = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set connection_type
     *
     * @param Swisscom\Matrix\StoreBundle\Entity\connection_type $connectionType
     */
    public function setConnectionType(\Swisscom\Matrix\StoreBundle\Entity\connection_type $connectionType)
    {
        $this->connection_type = $connectionType;
    }

    /**
     * Get connection_type
     *
     * @return Swisscom\Matrix\StoreBundle\Entity\connection_type 
     */
    public function getConnectionType()
    {
        return $this->connection_type;
    }

    /**
     * Set config_template
     *
     * @param Swisscom\Matrix\StoreBundle\Entity\config_template $configTemplate
     */
    public function setConfigTemplate(\Swisscom\Matrix\StoreBundle\Entity\config_template $configTemplate)
    {
        $this->config_template = $configTemplate;
    }

    /**
     * Get config_template
     *
     * @return Swisscom\Matrix\StoreBundle\Entity\config_template 
     */
    public function getConfigTemplate()
    {
        return $this->config_template;
    }

    /**
     * Set loopback
     *
     * @param Swisscom\Matrix\StoreBundle\Entity\loopback $loopback
     */
    public function setLoopback(\Swisscom\Matrix\StoreBundle\Entity\loopback $loopback)
    {
        $this->loopback = $loopback;
    }

    /**
     * Get loopback
     *
     * @return Swisscom\Matrix\StoreBundle\Entity\loopback 
     */
    public function getLoopback()
    {
        return $this->loopback;
    }

    /**
     * Add params
     *
     * @param Swisscom\Matrix\StoreBundle\Entity\parameters_form $params
     */
    public function addparameters_form(\Swisscom\Matrix\StoreBundle\Entity\parameters_form $params)
    {
        $this->params[] = $params;
    }

    /**
     * Get params
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getParams()
    {
        return $this->params;
    }
}