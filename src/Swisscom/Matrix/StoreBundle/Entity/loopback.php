<?php

namespace Swisscom\Matrix\StoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Swisscom\Matrix\StoreBundle\Entity\loopback
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Swisscom\Matrix\StoreBundle\Entity\loopbackRepository")
 */
class loopback
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
	 * @ORM\OneToMany(targetEntity="config_store", mappedBy="loopback")
	 */
	protected $config_store;

    /**
     * @var string $ip_address
     *
     * @ORM\Column(name="ip_address", type="string", length=255)
     */
    private $ip_address;

    /**
     * @var string $mask
     *
     * @ORM\Column(name="mask", type="string", length=255)
     */
    private $mask;

	public function __construct() {
		$this->config_store = new Collections\ArrayCollection();
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip_address
     *
     * @param string $ipAddress
     */
    public function setIpAddress($ipAddress)
    {
        $this->ip_address = $ipAddress;
    }

    /**
     * Get ip_address
     *
     * @return string 
     */
    public function getIpAddress()
    {
        return $this->ip_address;
    }

    /**
     * Set mask
     *
     * @param string $mask
     */
    public function setMask($mask)
    {
        $this->mask = $mask;
    }

    /**
     * Get mask
     *
     * @return string 
     */
    public function getMask()
    {
        return $this->mask;
    }

    /**
     * Add config_store
     *
     * @param Swisscom\Matrix\StoreBundle\Entity\config_store $configStore
     */
    public function addconfig_store(\Swisscom\Matrix\StoreBundle\Entity\config_store $configStore)
    {
        $this->config_store[] = $configStore;
    }

    /**
     * Get config_store
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getConfigStore()
    {
        return $this->config_store;
    }
}