<?php

namespace Swisscom\Matrix\StoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Swisscom\Matrix\StoreBundle\Entity\parameters_form
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Swisscom\Matrix\StoreBundle\Entity\parameters_formRepository")
 */
class parameters_form
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string $value
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;
    
    /**
     *  @ORM\ManyToMany(targetEntity="config_store", inversedBy="params")
     *  
     */
    protected $configs;

	public function __construct() {
        $this->configs = new \Doctrine\Common\Collections\ArrayCollection();
    }    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set value
     *
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add configs
     *
     * @param Swisscom\Matrix\StoreBundle\Entity\config_store $configs
     */
    public function addconfig_store(\Swisscom\Matrix\StoreBundle\Entity\config_store $configs)
    {
        $this->configs[] = $configs;
    }

    /**
     * Get configs
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getConfigs()
    {
        return $this->configs;
    }
}