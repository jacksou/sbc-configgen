<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$collection = new RouteCollection();
$collection->add('SwisscomMatrixHomeBundle_homepage', new Route('/Matrix/Home/{name}', array(
    '_controller' => 'SwisscomMatrixHomeBundle:Default:index',
)));

$collection->add('SwisscomMatrixHomeBundle_homepage', new Route('/Matrix/Hello', array(
    '_controller' => 'SwisscomMatrixHomeBundle:Default:hello',
)));

return $collection;
