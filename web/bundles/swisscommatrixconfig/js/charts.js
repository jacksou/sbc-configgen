/*!
 * Ext JS Library 3.3.1
 * Copyright(c) 2006-2010 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */
Ext.chart.Chart.CHART_URL = 'extjs/resources/charts.swf';

Ext.onReady(function(){
    
    var storeView = new Ext.data.JsonStore({
    	url: 'page/handler_view.php',
        fields:['name', 'daily', 'weekly'],
    });
	storeView.load();
	
	var storeUsers = new Ext.data.JsonStore({
    	url: 'page/handler_users.php',
        fields:['name', 'daily', 'weekly'],
    });
	storeUsers.load();

	var storeTime = new Ext.data.JsonStore({
    	url: 'page/handler_time.php',
        fields:['name', 'daily', 'weekly'],
    });
	storeTime.load();
  
    // Channels view (daily and weekly)
    new Ext.Panel({
        iconCls:'chart',
        title: 'Channels view (daily and weekly) [view/channel]',
        frame:true,
        renderTo: 'charts_view',
        width:680,
        height:500,
        layout:'fit',

        items: {
            xtype: 'columnchart',
            store: storeView,
            url:'extjs/resources/charts.swf',
            xField: 'name',
            yAxis: new Ext.chart.NumericAxis({
                displayName: 'weekly',
                labelRenderer : Ext.util.Format.numberRenderer('0,0')
            }),
            tipRenderer : function(chart, record, index, series){
                if(series.yField == 'daily'){
                    return Ext.util.Format.number(record.data.daily, '0,0') + ' view daily for ' + record.data.name;
                }else{
                    return Ext.util.Format.number(record.data.weekly, '0,0') + ' view weekly for ' + record.data.name;
                }
            },
            chartStyle: {
                padding: 10,
                animationEnabled: true,
                font: {
                    name: 'Tahoma',
                    color: 0x444444,
                    size: 11
                },
                dataTip: {
                    padding: 5,
                    border: {
                        color: 0x99bbe8,
                        size:1
                    },
                    background: {
                        color: 0xDAE7F6,
                        alpha: .9
                    },
                    font: {
                        name: 'Tahoma',
                        color: 0x15428B,
                        size: 10,
                        bold: true
                    }
                },
                xAxis: {
                    color: 0x69aBc8,
                    majorTicks: {color: 0x69aBc8, length: 4},
                    minorTicks: {color: 0x69aBc8, length: 2},
                    majorGridLines: {size: 1, color: 0xeeeeee}
                },
                yAxis: {
                    color: 0x69aBc8,
                    majorTicks: {color: 0x69aBc8, length: 4},
                    minorTicks: {color: 0x69aBc8, length: 2},
                    majorGridLines: {size: 1, color: 0xdfe8f6}
                }
            },
            series: [{
                type: 'column',
                displayName: 'Page daily',
                yField: 'daily',
                style: {
                    image:'bar.gif',
                    mode: 'stretch',
                    color:0x99BBE8
                }
            },{
                type:'line',
                displayName: 'weekly',
                yField: 'weekly',
                style: {
                    color: 0x15428B
                }
            }],
            extraStyle: {
                xAxis: {
                     labelRotation: -90
                 }
             }
        }
    });
    
    
    
    // Channels Users (daily and weekly)
    new Ext.Panel({
        iconCls:'chart',
        title: 'Channels Users (daily and weekly) [user/channel]',
        frame:true,
        renderTo: 'charts_user',
        width:680,
        height:500,
        layout:'fit',

        items: {
            xtype: 'columnchart',
            store: storeUsers,
            url:'extjs/resources/charts.swf',
            xField: 'name',
            yAxis: new Ext.chart.NumericAxis({
                displayName: 'weekly',
                labelRenderer : Ext.util.Format.numberRenderer('0,0')
            }),
            tipRenderer : function(chart, record, index, series){
                if(series.yField == 'daily'){
                    return Ext.util.Format.number(record.data.daily, '0,0') + ' users daily for ' + record.data.name;
                }else{
                    return Ext.util.Format.number(record.data.weekly, '0,0') + ' users weekly for ' + record.data.name;
                }
            },
            chartStyle: {
                padding: 10,
                animationEnabled: true,
                font: {
                    name: 'Tahoma',
                    color: 0x444444,
                    size: 11
                },
                dataTip: {
                    padding: 5,
                    border: {
                        color: 0x99bbe8,
                        size:1
                    },
                    background: {
                        color: 0xDAE7F6,
                        alpha: .9
                    },
                    font: {
                        name: 'Tahoma',
                        color: 0x15428B,
                        size: 10,
                        bold: true
                    }
                },
                xAxis: {
                    color: 0x69aBc8,
                    majorTicks: {color: 0x69aBc8, length: 4},
                    minorTicks: {color: 0x69aBc8, length: 2},
                    majorGridLines: {size: 1, color: 0xeeeeee}
                },
                yAxis: {
                    color: 0x69aBc8,
                    majorTicks: {color: 0x69aBc8, length: 4},
                    minorTicks: {color: 0x69aBc8, length: 2},
                    majorGridLines: {size: 1, color: 0xdfe8f6}
                }
            },
            series: [{
                type: 'column',
                displayName: 'Page daily',
                yField: 'daily',
                style: {
                    image:'bar.gif',
                    mode: 'stretch',
                    color:0x99BBE8
                }
            },{
                type:'line',
                displayName: 'weekly',
                yField: 'weekly',
                style: {
                    color: 0x15428B
                }
            }],
            extraStyle: {
                xAxis: {
                     labelRotation: -90
                 }
             }
        }
    });
    
    
    
 // Channels Times (daily and weekly)
    new Ext.Panel({
        iconCls:'chart',
        title: 'Channels Time (daily and weekly) [hour/channel]',
        frame:true,
        renderTo: 'charts_time',
        width:680,
        height:500,
        layout:'fit',

        items: {
            xtype: 'columnchart',
            store: storeTime,
            url:'extjs/resources/charts.swf',
            xField: 'name',
            yAxis: new Ext.chart.NumericAxis({
                displayName: 'weekly',
                labelRenderer : Ext.util.Format.numberRenderer('0.0')
            }),
            tipRenderer : function(chart, record, index, series){
                if(series.yField == 'daily'){
                    return Ext.util.Format.number(record.data.daily, '0.0') + ' hours daily for ' + record.data.name;
                }else{
                    return Ext.util.Format.number(record.data.weekly, '0.0') + ' hours weekly for ' + record.data.name;
                }
            },
            chartStyle: {
                padding: 10,
                animationEnabled: true,
                font: {
                    name: 'Tahoma',
                    color: 0x444444,
                    size: 11
                },
                dataTip: {
                    padding: 5,
                    border: {
                        color: 0x99bbe8,
                        size:1
                    },
                    background: {
                        color: 0xDAE7F6,
                        alpha: .9
                    },
                    font: {
                        name: 'Tahoma',
                        color: 0x15428B,
                        size: 10,
                        bold: true
                    }
                },
                xAxis: {
                    color: 0x69aBc8,
                    majorTicks: {color: 0x69aBc8, length: 4},
                    minorTicks: {color: 0x69aBc8, length: 2},
                    majorGridLines: {size: 1, color: 0xeeeeee}
                },
                yAxis: {
                    color: 0x69aBc8,
                    majorTicks: {color: 0x69aBc8, length: 4},
                    minorTicks: {color: 0x69aBc8, length: 2},
                    majorGridLines: {size: 1, color: 0xdfe8f6}
                }
            },
            series: [{
                type: 'column',
                displayName: 'Page daily',
                yField: 'daily',
                style: {
                    image:'bar.gif',
                    mode: 'stretch',
                    color:0x99BBE8
                }
            },{
                type:'line',
                displayName: 'weekly',
                yField: 'weekly',
                style: {
                    color: 0x15428B
                }
            }],
            extraStyle: {
                xAxis: {
                     labelRotation: -90
                 }
             }
        }
    });
    
});